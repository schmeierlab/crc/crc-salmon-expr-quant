# PROJECT: crc-salmon-expr-quant
AUTHOR: Sebastian Schmeier (s.schmeier@gmail.com) 
DATE: 2018-09-27

## OVERVIEW

We make use of Salmon to quantify transcript expression.
Gene expression values in tags-per-million are then dereived using tximport package.

## INSTALL

```bash
# Install miniconda
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# MACOSX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh

# Add conda dir to PATH
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.zshrc

# Make env
# this environment contains the bare base of what is required to run snakemake
conda env create --name snakemake --file envs/snakemake.yaml
conda activate snakemake
```

## Execute workflow

```bash
snakemake -p --use-conda --use-singularity --singularity-args "--bind /mnt/disk2" --jobs 12 > logs/run.stdout 2> logs/run.stderr
```