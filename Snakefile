import glob, os, os.path
from os.path import join

## define global Singularity image for reproducibility
## needs --use-singularity to run all jobs in container
singularity: "docker://continuumio/miniconda3:4.5.4"

## For data download
from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
FTP = FTPRemoteProvider()

## LOAD VARIABLES FROM CONFIGFILE ----------
configfile: "config.yml"

BASEDIR = config["basedir"]
LOGDIR = config["logdir"]
BENCHMARKDIR = config["benchmarkdir"]
WRAPPERDIR = config["wrapperdir"]
SCRIPTDIR = config["scriptdir"]
ENVS = config["envs"]

## INPUTS
SAMPLEDIR = config["sampledir"]
GDIR = config["genomedir"]
GENOME = config["genome"]
GENOMEFTP = config["genomeftp"]
GTF = config["gtf"]
GTFFTP = config["gtfftp"]
GENE2ENS = config["gene2ensembl"]
GENE2ENSFTP = config["gene2ensemblftp"]

PATTERN_1 = config["pattern_read1"]
PATTERN_2 = config["pattern_read2"]

MAP = join(GDIR,"tx_gene_map.txt")

## OUTPUTS
QUANTDIR = config["quantdir"]
##------------------------------------------

## sample basenames
SAMPLES, S2 = glob_wildcards(join(SAMPLEDIR+'/{s1}', PATTERN_1))
TARGETS = expand(join(QUANTDIR, '{sample}'), sample=SAMPLES)
print(len(SAMPLES))
#print(SAMPLES)

## Pseudo-rule to list the final targets,
## so that the whole workflow is run.
rule all:
    input:
        join(QUANTDIR, "tximport_lengthScaledTPM_entrezgene.txt.gz")

        
rule get_genome:
    input:
        FTP.remote(GENOMEFTP, keep_local=True)
    output:
        join(GDIR, GENOME)
    run:
        shell("mv {input} {output}")

        
rule get_gtf:
    input:
        FTP.remote(GTFFTP, keep_local=True)
    output:
        join(GDIR, GTF)
    run:
        shell("mv {input} {output}")

        
rule salmon_idx:
    input:
        g=join(GDIR, GENOME)
    output:
        directory('%s.idx' % join(GDIR, GENOME))
    log:
        log1=join(LOGDIR, "salmon_idx/stdout"),
	log2=join(LOGDIR, "salmon_idx/stderr")
    benchmark:
        join(BENCHMARKDIR,"salmon_idx.txt")
    threads: 12
    conda:
        join(ENVS, "salmon.yaml")
    shell:
        "salmon index -p {threads} -t <(gunzip -c {input.g}) -i {output} > {log.log1} 2> {log.log2}"
        
        
rule salmon_pe:
    input:
        sample = [join(SAMPLEDIR+'/{sample}', PATTERN_1),
                  join(SAMPLEDIR+'/{sample}', PATTERN_2)],
        index='%s.idx' % join(GDIR, GENOME)
    output:
        directory(join(QUANTDIR, "{sample}"))
    log:
        log1=join(LOGDIR, "salmon_pe/{sample}.stdout"),
        log2=join(LOGDIR, "salmon_pe/{sample}.stderr")
    benchmark:
        join(BENCHMARKDIR,"{sample}.salmon_pe.txt")
    params:
        extra=r"-l A --gcBias"
    threads: 4
    conda:
        join(ENVS, "salmon.yaml")
    shell:
        "salmon quant {params.extra} -i {input.index} -1 <(gunzip -c {input.sample[0]}) -2 <(gunzip -c {input.sample[1]}) -o {output} -p {threads} > {log.log1} 2> {log.log2}"


rule sample_list:
    input:
        TARGETS
    output:
        join(QUANTDIR, "samples.txt")
    log:
        join(LOGDIR, "sample_list.log")
    params:
         dir=QUANTDIR
    shell:
        'for i in $(find {params.dir} -name "quant.sf" -print); do name=$(basename $(dirname $i)); echo -e "Group1\tRep1\t$name\t$i"; done > {output} 2> {log}'


rule create_txmap:
    input:
        join(GDIR, GTF)
    output:
        MAP
    params:
        script = join(SCRIPTDIR, "txmap.py")
    shell:
        """zcat {input} | python {params.script} - > {output}"""

        
rule tximport:
    input:
        samplelist=join(QUANTDIR, "samples.txt"),
        map=MAP
    output:
        join(QUANTDIR, "tximport_lengthScaledTPM.txt.gz")
    log:
        join(LOGDIR, "tximport.log")
    params:
        scaling="lengthScaledTPM",
        log="no",
        alntype="salmon"
    conda:
        join(ENVS, "tximport.yaml")
    shell:
        "Rscript src/scripts/pseudoaln_get_genes_TPM.R {params.alntype} {params.scaling} {params.log} {input.map} {input.samplelist} {output} 2> {log}"


rule get_gene2ensembl:
    input:
        FTP.remote(GENE2ENSFTP, keep_local=True)
    output:
        join(GDIR, GENE2ENS)
    shell:
        "mv {input} {output}"


rule entrezgene:
    input:
        tximports=join(QUANTDIR, "tximport_lengthScaledTPM.txt.gz"),
        gene2ensembl=join(GDIR, GENE2ENS)
    output:
        os.path.join(QUANTDIR, "tximport_lengthScaledTPM_entrezgene.txt.gz")
    benchmark:
        join(BENCHMARKDIR, 'entrezgene.txt')
    log:
        log1 = join(LOGDIR, "entrezgene/entrezgene.stdout"),
        log2 = join(LOGDIR, "entrezgene/entrezgene.stderr")
    shell:
        "python src/scripts/subselect_entrezgene.py {input.tximports} {input.gene2ensembl} -o {output} > {log.log1} 2> {log.log2}"


rule clean:
    shell:
        "rm -rf {BASEDIR}/*"
        
        
