#!/usr/bin/env python
"""

VERSION HISTORY

0.1  2018/08/22    Init

"""
__version__='0.1'
__date__='2018/08/22'
__email__='s.schmeier@gmail.com'
__author__='Sebastian Schmeier'
import sys, gzip, csv, re, bz2, zipfile
import argparse

def parse_args():
    
    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Create tx to gene maop from gtf'
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    oParser = argparse.ArgumentParser(description=sDescription,
                                      epilog=sEpilog)

    oParser.add_argument('sFile',
                         metavar='FILE',
                         help='Delimited file. [if set to "-" reads from standard in]')
    oParser.add_argument('-o', '--out',
                         metavar='STRING',
                         dest='sOut',
                         default=None,
                         help='Out-file. [default: stdout]')
    oArgs = oParser.parse_args()
    return oArgs, oParser


def load_file(s):
    if s in ['-', 'stdin']:
        oF = sys.stdin
    elif s.split('.')[-1] == 'gz':
        oF = gzip.open(s)
    elif s.split('.')[-1] == 'bz2':
        oF = bz2.BZFile(s)
    elif s.split('.')[-1] == 'zip':
        oF = zipfile.Zipfile(s)
    else:
        oF = open(s)
    return oF

def main():
    oArgs, oParser = parse_args()

    if not oArgs.sOut:
        oFout = sys.stdout
    elif oArgs.sOut in ['-', 'stdin']:
        oFout = sys.stdout
    elif oArgs.sOut.split('.')[-1] == 'gz':
        oFout = gzip.open(oArgs.sOut, 'wb')
    else:
        oFout = open(oArgs.sOut, 'w')
    
    oF = load_file(oArgs.sFile)

    oReg =  re.compile('(ENSG.+?)".+(ENST.+?)".+transcript_version\s"(.+?)".+gene_name\s"(.+?)"')

    a = []
    for s in oF:
        oRes = oReg.search(s)
        if oRes:
            tx = '{}.{}\t{}\t{}'.format(oRes.group(2), oRes.group(3), oRes.group(1), oRes.group(4))
            a.append(tx)
            
    a = list(set(a))
    a.sort()
    for s in a:
        oFout.write('{}\n'.format(s))
            
if __name__ == '__main__':
    main()

