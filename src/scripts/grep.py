#!/usr/bin/env python
"""

VERSION HISTORY

0.1.2  2017/07/08    Py3 ready
0.1    2011/08/10    Initial version.

"""
__version__='0.1.2'
__date__='2017/07/108'
__email__='s.schmeier@gmail.com'
__author__='Sebastian Schmeier'
import sys, gzip, csv, re, bz2, zipfile
import argparse

def parse_args():
    
    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Read file and search for regular expression. Print line or groups if a match occurrs.' 
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    oParser = argparse.ArgumentParser(description=sDescription,
                                      epilog=sEpilog)
    oParser.add_argument('--version',
                        action='version',
                        version='%s' % (sVersion))
    oParser.add_argument('sFile',
                         metavar='FILE',
                         help='Delimited file. [if set to "-" reads from standard in]')
    oParser.add_argument('sReg',
                         metavar='STRING',
                         help='Regular expression to search. Rounded brackets are groups that can be extracted.')
    oParser.add_argument('-o', '--out',
                         metavar='STRING',
                         dest='sOut',
                         default=None,
                         help='Out-file. [default: stdout]')
    oParser.add_argument('-d', '--delimiter',
                         metavar='STRING',
                         dest='sSEP',
                         default='\t',
                         help='Delimiter used in file.  [default: "tab"]')
    oParser.add_argument('-i', '--inverse',
                         action='store_true',
                         dest='bI',
                         default=False,
                         help='Print lines that are not macthcing the regexp.')
    oParser.add_argument('-l', '--line',
                         action='store_true',
                         dest='bLine',
                         default=False,
                         help='Print the matched group and then tab seperated the line where it was found.')
    ## oParser.add_argument('-g', '--groups',
    ##                      metavar='STRING',
    ##                      dest='sGroups',
    ##                      default='1',
    ##                      help='Groups to extract. e.g. -g "1,2" --> extract the first and second group  [default: "all that are sepecified in regular expression"]')

    oArgs = oParser.parse_args()
    return oArgs, oParser


def load_file(s):
    if s in ['-', 'stdin']:
        oF = sys.stdin
    elif s.split('.')[-1] == 'gz':
        oF = gzip.open(s)
    elif s.split('.')[-1] == 'bz2':
        oF = bz2.BZFile(s)
    elif s.split('.')[-1] == 'zip':
        oF = zipfile.Zipfile(s)
    else:
        oF = open(s)
    return oF

def main():
    oArgs, oParser = parse_args()

    if not oArgs.sOut:
        oFout = sys.stdout
    elif oArgs.sOut in ['-', 'stdin']:
        oFout = sys.stdout
    elif oArgs.sOut.split('.')[-1] == 'gz':
        oFout = gzip.open(oArgs.sOut, 'wb')
    else:
        oFout = open(oArgs.sOut, 'w')
    
    oF = load_file(oArgs.sFile)

    try:
        oReg =  re.compile(oArgs.sReg)
    except:
        sys.stderr.write('ERROR. Not a valid regular expression. Exit.\n')
        sys.exit()

    for s in oF:
        oRes = oReg.search(s)
        if oArgs.bLine:
            sLine = '\t'+s.strip()
        else:
            sLine = ''
        if oRes:
            if oArgs.bI: continue
            if len(oRes.groups()) > 0:
                oFout.write('%s%s\n'%('\t'.join(list(oRes.groups())), sLine))
            else:
                oFout.write('%s%s\n'%(s.strip(), sLine))
        else:
            if oArgs.bI:
                oFout.write('%s\n'%(s))
            
if __name__ == '__main__':
    main()

