#!/usr/bin/env python
"""
NAME: subselect_entrezgene
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1   20171209    Initial version.

LICENCE
=======
2017, copyright Sebastian Schmeier (s.schmeier@gmail.com)

template version: 1.6 (2016/11/09)
"""
from timeit import default_timer as timer
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.1'
__date__ = '20171209'
__email__ = 's.schmeier@gmail.com'
__author__ = 'Sebastian Schmeier'


def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Attach gene_ids to ensembl ids and subselect all genes with non-redundant relationship (i.e. 1-to-1).'
    version = 'version %s, date %s' % (__version__, __date__)
    epilog = 'Copyright %s (%s)' % (__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s' % (version))

    parser.add_argument(
        'str_file',
        metavar='FILE',
        help=
        'Delimited file. [if set to "-" or "stdin" reads from standard in]')
    parser.add_argument(
        'str_ref',
        metavar='FILE',
        help= 'NCBI gene2ensembl REF (ftp://ftp.ncbi.nih.gov/gene/DATA/gene2ensembl.gz)')

    parser.add_argument('-o',
                        '--out',
                        metavar='STRING',
                        dest='outfile_name',
                        default=None,
                        help='Out-file. [default: "stdout"]')

    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle

def main():
    """ The main funtion. """
    args, parser = parse_cmdline()
    fileobj = load_file(args.str_file)
    fileref = load_file(args.str_ref)

    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ['-', 'stdout']:
        outfileobj = sys.stdout
    elif args.outfile_name.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.outfile_name, 'wt')
    else:
        outfileobj = open(args.outfile_name, 'wt')
        
    reader = csv.reader(fileobj, delimiter = '\t')
    readerref = csv.reader(fileref, delimiter = '\t')
    header = next(readerref)
    uniqids = {}
    for a in readerref:
        uniqids[(a[1],a[2])] = None

    # redundant
    entrezred = {} 
    ensemblred = {}
    e1 = {}
    e2 = {}
    for entrezid, ensemblid in uniqids:
        if entrezid in e1:
            entrezred[entrezid] = None
        if ensemblid in e2:
            ensemblred[ensemblid] = None
        e1[entrezid] = None
        e2[ensemblid] = None
    
    d = {}
    for entrezid, ensemblid in uniqids:
        if entrezid in entrezred:
            continue
        if ensemblid in ensemblred:
            continue
        d[ensemblid] = entrezid

    
        
    outfileobj.write('{}\n'.format('\t'.join(next(reader))))
    for a in reader:
        if a[0] not in d:
            continue
        a[0] = d[a[0]]
        outfileobj.write('{}\n'.format('\t'.join(a)))
        
        
    # ------------------------------------------------------
    outfileobj.close()
    sys.stderr.write('{} entrez; {} ensemble without 1-to-1 relationship => discarded.\n'.format(len(entrezred), len(ensemblred)))
    for e in list(entrezred.keys())+list(ensemblred.keys()):
        sys.stderr.write('{}\n'.format(e))
        
    return


if __name__ == '__main__':
    sys.exit(main())

