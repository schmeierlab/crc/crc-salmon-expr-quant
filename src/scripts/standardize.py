#!/usr/bin/env python
"""
NAME: seb.DATA.standardize.py
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

v0.1   2016/11/09    Initial version.

LICENCE
=======
2016, copyright Sebastian Schmeier (s.schmeier@gmail.com), sschmeier.com

template version: 1.5 (2016/09/09)
"""
from timeit import default_timer as timer
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time
## non standard libraries to be imported
import numpy as np
import pandas as pd  

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = 'v0.1'
__date__ = '2016/11/09'
__email__ = 's.schmeier@gmail.com'
__author__ = 'Sebastian Schmeier'


def parse_cmdline():
    """ Parse command-line args."""
    ## parse cmd-line -----------------------------------------------------------
    description = "Read delimited data-file and calculate z-scores. It calculates per row a zscore for each column as Z = (value - mean_col) / std_col, based on the respective col-mean and col-std. '--method row' can be used to standardise over rows instead of columns, i.e. Z = (value - mean_row) / std_row."
    version = 'version %s, date %s' % (__version__, __date__)
    epilog = 'Copyright %s (%s)' % (__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s' % (version))

    parser.add_argument(
        'str_file',
        metavar='FILE',
        help=
        'Delimited data file to produce zscores for. [if set to "-" or "stdin" reads from standard in]')
    parser.add_argument('-a',
                        '--header',
                        dest='header_exists',
                        action='store_true',
                        default=False,
                        help='Header in File. [default: False]')
    parser.add_argument('-d',
                        '--delimiter',
                        metavar='STRING',
                        dest='delimiter_str',
                        default='\t',
                        help='Delimiter used in file.  [default: "tab"]')
    parser.add_argument('-o',
                        '--out',
                        metavar='STRING',
                        dest='outfile_name',
                        default=None,
                        help='Out-file. [default: "stdout"]')
    parser.add_argument('-m',
                        '--method',
                        metavar='STRING',
                        dest='method',
                        default='col',
                        help='Column-wise or row-wise. [default: "col"]')
    
    # PANDAS
    parser.add_argument('-l',
                        '--labels',
                        dest='label_number',
                        metavar="INT",
                        type=int,
                        default=None,
                        help='Column number to use as labels. [default: None]')

    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
        
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename)
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ['-', 'stdout']:
        outfileobj = sys.stdout
    elif args.outfile_name.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.outfile_name, 'wb')
    else:
        outfileobj = open(args.outfile_name, 'w')
    
    # ------------------------------------------------------
    # PANDAS approach load data and clean
    fileobj = load_file(args.str_file)

    # Check labels
    if args.label_number:
        if args.label_number <= 0:
            parser.error('Label column number has to be > 0. EXIT.')
        label_number = args.label_number - 1
    else:
        label_number = False

    if args.header_exists:
        df = pd.read_csv(fileobj,
                         sep=args.delimiter_str,
                         index_col=label_number)
    else:
        df = pd.read_csv(fileobj,
                         sep=args.delimiter_str,
                         index_col=label_number,
                         header=None)
    
    # replace empty values with NaN
    df = df.replace(r'\s+', np.nan, regex=True)
  
    # ------------------------------------------------------
    # DO SOME WORK
    # calculate z-score for each data point based on column (row) mean and std

    if args.method == 'col':
        df = (df - df.mean(axis=0)) /  df.std(axis=0)
    else:
        df = df.sub(df.mean(axis=1), axis=0).div(df.std(axis=1), axis=0)
    
    df.to_csv(outfileobj, sep=args.delimiter_str)

    # ------------------------------------------------------
    outfileobj.close()
    return


if __name__ == '__main__':
    sys.exit(main())

